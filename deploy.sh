#!/bin/bash

###########################################################
#
#	Titre 	: deploy.sh
#	
#	Auteur	: DD
#
#	Date	: 13/02/2022
#
###########################################################

#Si option --create
if [ "$1" == "--create" ];then

	#definition du nombre de conteneur	
	nb_machine=1
	[ "$2" != "" ] && nb_machine=$2

	#setting de min et de max 
	min=1
	max=0

	#récupération de l'id max
	idmax=`docker ps -a | grep ubuntu | awk -F"-" '{print $3}'|sort -n|tail -1`
	
	#redifinition de min et max
	min=$(($idmax + 1))
	max=$(($idmax + $nb_machine))

	#Création des conteneurs
	echo "Début de la création du/des conteneurs..."
	for i in $(seq $min $max);do
		docker run -tid --cap-add NET_ADMIN --cap-add SYS_ADMIN --publish-all=true -v /srv/data:/srv/html -v /sys/fs/cgroup:/sys/fs/cgroup:ro --name $USER-debian-$i -h $USER-debian-$i registry.gitlab.com/donodono/scripting-bash/debian-systemd:v1.0
		docker exec -ti $USER-debian-$i /bin/sh -c "useradd -m -p sa3tHJ3/KuYvI $USER"
		docker exec -ti $USER-debian-$i /bin/sh -c "mkdir  ${HOME}/.ssh && chmod 700 ${HOME}/.ssh && chown $USER:$USER $HOME/.ssh"
		docker cp $HOME/.ssh/id_rsa.pub $USER-debian-$i:$HOME/.ssh/authorized_keys
		docker exec -ti $USER-debian-$i /bin/sh -c "chmod 600 ${HOME}/.ssh/authorized_keys && chown $USER:$USER $HOME/.ssh/authorized_keys"
		docker exec -ti $USER-debian-$i /bin/sh -c "echo '$USER   ALL=(ALL) NOPASSWD: ALL'>>/etc/sudoers"
		docker exec -ti $USER-debian-$i /bin/sh -c "service ssh start"

		echo "Conteneur $USER-debian-$i crée"
	done

	echo "${nb_machine} machine(s) virtuelle(s) lancée(s)"

#Si option --drop	
elif [ "$1" == "--drop" ];then
	echo "Suppression des conteneurs..."
	docker rm -f $(docker ps -a | grep $USER-debian | awk '{print $1}')
	echo "fin de suppresision des conteneurs"

#Si option --start
elif [ "$1" == "--start" ];then
	echo ""
	docker start $(docker ps -a | grep $USER-debian | awk '{print $1}')
	echo ""

#Si option --info
elif [ "$1" == "--info" ];then
	
	echo ""
	echo "Informations des conteneurs : " 
	echo ""
	for conteneur in $(docker ps -a | grep $USER-debian | awk '{print $1}');do
		docker inspect -f ' => {{.Name}} - {{.NetworkSettings.IPAddress}}' $conteneur
	done
	echo ""

#Si option --ansible
elif [ "$1" == "--ansible" ];then
	echo ""
	echo " notre option est --ansible"
	echo "" 

else

#Si aucune option affichage de l'aide
echo "

	Options :

	--create : si aucun chiffre 2 conteneurs sinon x conteneurs seront créés

	--drop : pour supprimer les conteneurs créés via ce script par ${USER}

	--start : pour redémarrer les conteneurs

	--infos : pour récapituler les infos des conteneurs (user, ip, nom...)

	--ansible : pour créer une base de dev pour ansible

"

fi
